
//% color="#ff9f06" iconWidth=50 iconHeight=40
namespace pinpong_huskylens{
  

    //% block="Huskylens begin I2C" blockType="command"
    export function huskylensBegin(parameter: any, block: any) {
        

        Generator.addImport('from pinpong.libs.dfrobot_huskylens import Huskylens');
        Generator.addCode(`husky = Huskylens()`);
        Generator.addCode(`husky.command_request()`);

        
    }


    //% block="Huskylens switch mode to [MODE]" blockType="command"
    //% MODE.shadow="dropdown"  MODE.options="MODE"    
    export function command_request_algorthim(parameter: any, block: any) {
        var mode=parameter.MODE.code;
        
        Generator.addCode(`husky.command_request_algorthim("${mode}")`);
        
    }   


    //% block="---"
    export function noteSep1() {

    }

    //% block="Huskylens request" blockType="reporter"
    export function command_request(parameter: any, block: any) {
    
        Generator.addCode(`husky.command_request()`);
        
    }
/*
    //% block="Huskylens command_request_blocks " blockType="reporter"
    export function command_request_blocks(parameter: any, block: any) {
    
        Generator.addCode(`husky.command_request_blocks()`);
        
    }

    //% block="Huskylens command_request_arrows " blockType="reporter"
    export function command_request_arrows(parameter: any, block: any) {

        Generator.addCode(`husky.command_request_arrows()`);
        
    }
*/
    //% block="Huskylens command_request_learned " blockType="reporter"
    export function command_request_learned(parameter: any, block: any) {

        Generator.addCode(`husky.command_request_learned()`);
        
    }
/*
    //% block="Huskylens command_request_blocks_learned " blockType="reporter"
    export function command_request_blocks_learned(parameter: any, block: any) {

        Generator.addCode(`husky.command_request_blocks_learned()`);
        
    }

    //% block="Huskylens command_request_arrows_learned " blockType="reporter"
    export function command_request_arrows_learned(parameter: any, block: any) {

        Generator.addCode(`husky.command_request_arrows_learned()`);
        
    }
*/

    //% block="Huskylens command_request_by_id  [ID] " blockType="reporter"
    //% ID.shadow="number" ID.defl=1
    export function command_request_by_id(parameter: any, block: any) {
        var id=parameter.ID.code;
        Generator.addCode(`husky.command_request_by_id(${id})`);
        
    }
/*
    //% block="Huskylens command_request_blocks_by_id  [ID] " blockType="reporter"
    //% ID.shadow="number" ID.defl=1
    export function command_request_blocks_by_id(parameter: any, block: any) {
        var id=parameter.ID.code;
        Generator.addCode(`husky.command_request_blocks_by_id(${id})`);
        
    }

    //% block="Huskylens command_request_arrows_by_id  [ID] " blockType="reporter"
    //% ID.shadow="number" ID.defl=1
    export function command_request_arrows_by_id(parameter: any, block: any) {
        var id=parameter.ID.code;
        Generator.addCode(`husky.command_request_arrows_by_id(${id})`);
        
    }
*/

    //% block="---"
    export function noteSep2() {

    }

    //% block="Huskylens [ID] learn once" blockType="command"
    //% ID.shadow="number" ID.defl=1
    export function command_request_learn_once(parameter: any, block: any) {
        var id=parameter.ID.code;
        
        Generator.addCode(`husky.command_request_learn_once(${id}) `);
        
    }   

    //% block="Huskylens forget" blockType="command"
    export function command_request_forget(parameter: any, block: any) {
        
        Generator.addCode(`husky.command_request_forget() `);
        
    }   

    //% block="---"
    export function noteSep4() {

    }

    //% block="Huskylens draw text[TEXT] x:[X] y:[Y] " blockType="command"
    //% TEXT.shadow="string" TEXT.defl="Mind+"
    //% X.shadow="range"   X.params.min=0    X.params.max=320    X.defl=25
    //% Y.shadow="range"   Y.params.min=0    Y.params.max=240    Y.defl=52
    export function command_request_custom_text(parameter: any, block: any) {
        var text=parameter.TEXT.code;
        var x=parameter.X.code;
        var y=parameter.Y.code;
        
        Generator.addCode(`husky.command_request_custom_text(${text},${x},${y})`);
        
    }   

    //% block="Huskylens command_request_clear_text " blockType="command"
    export function command_request_clear_text(parameter: any, block: any) {
        Generator.addCode(`husky.command_request_clear_text()`);
        
    }


    //% block="Huskylens set ID [ID] name [NAME] " blockType="command"
    //% ID.shadow="number" ID.defl=1
    //% NAME.shadow="string" NAME.defl="Mind+"
    export function command_request_customnames(parameter: any, block: any) {
        var id=parameter.ID.code;
        var name=parameter.NAME.code;
        
        Generator.addCode(`husky.command_request_customnames(${id},${name}) `);
        
    }   

    //% block="---"
    export function noteSep3() {

    }


    //% block="Huskylens save [IMG]" blockType="command"
    //% IMG.shadow="dropdown"  IMG.options="IMG"    
    export function command_request_img(parameter: any, block: any) {
        var img=parameter.IMG.code;
        
        Generator.addCode(`husky.command_request_${img}()`);
        
    }   

    
    //% block="Huskylens [SAVELOAD] model [INDEX] to SD card  " blockType="command"
    //% SAVELOAD.shadow="dropdown"  SAVELOAD.options="SAVELOAD"
    //% INDEX.shadow="dropdownRound"   INDEX.options="INDEX"     
    export function command_request_saveload_model_to_SD_card(parameter: any, block: any) {
        var sl=parameter.SAVELOAD.code;
        var index=parameter.INDEX.code;
        
        Generator.addCode(`husky.command_request_${sl}_model_to_SD_card(${index}) `);
        
    }   
    
    /*
    //% block="Huskylens knock " blockType="reporter"
    export function command_request_knock(parameter: any, block: any) {
    
        Generator.addCode(`husky.command_request_knock()`);
        
    }
    */


    function replaceQuotationMarks(str:string){
            str=str.replace(/"/g, ""); //去除所有引号
            return str
    }


    
}
